﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WooCommerceApiClient.Models;

namespace WooCommerceApiClient.Tests
{
    [TestClass]
    public class WooCommerceApiServiceTest
    {
        // Enter creds
        const string StoreUrl = "http://perfumancebd.com";
        const string CustKey = "ck_82eafc150627ccf20550888eb07dc1809bccaafa";
        const string CustSecret = "cs_8f50099596df44a8f17e27d3189c74d5ff584c0c";

        WooCommerceApiService _Service = WooCommerceApiService.Instance($"{ StoreUrl}/wp-json/wc/v1/", CustKey, CustSecret);

        [TestMethod]
        public void TestCanGetOrders()
        {
            List<Order> orders = _Service.GetOrdersAsync().Result;

            Assert.IsNotNull(orders);
        }

        [TestMethod]
        public void TestCanGetProducts()
        {
            List<Product> products = _Service.GetProductsAsync().Result;

            Assert.IsNotNull(products);
        }

        [TestMethod]
        public void TestCanGetOrdersAsString()
        {
            string result = _Service.GetOrdersAsStringAsync().Result;
            Assert.IsNotNull(result);
        }
    }
}
