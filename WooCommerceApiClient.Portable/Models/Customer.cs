﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceApiClient.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string last_order_id { get; set; }
        public string last_order_date { get; set; }
        public int orders_count { get; set; }
        public string total_spent { get; set; }
        public string avatar_url { get; set; }
        public BillingAddress2 billing_address { get; set; }
        public ShippingAddress2 shipping_address { get; set; }
    }
}
