﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceApiClient.Models
{
    public class PaymentDetails
    {
        public string method_id { get; set; }
        public string method_title { get; set; }
        public bool paid { get; set; }
    }
}
