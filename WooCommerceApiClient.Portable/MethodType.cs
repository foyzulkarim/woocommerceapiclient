﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceApiClient
{
    /// <summary>
    /// Enumeration representing request types
    /// </summary>
    public enum MethodType
    {
        GET,
        POST,
        PUT,
        HEAD
    }
}
