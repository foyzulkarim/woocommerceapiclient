#WooCommerceApiClient

This is a small helper Library to get orders and products from Woocommerce from a Xamarin PCL

##Features:

- Async

- HttpClient using basic authentication

##How to use the Lib

See the test project for how to use the library

##TODO

- Add other request methods eg POST

- Create Order and Product repository classes to easily handle CRUD operations

Author: Luke Warren